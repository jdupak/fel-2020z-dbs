DIAGRAM_TARGET = er-diagram
DESC_TARGET = er-description
RM = rm-schema

all: $(DIAGRAM_TARGET) $(DESC_TARGET) $(RM)

$(DIAGRAM_TARGET):
	dot src/$(DIAGRAM_TARGET).dot -Tpdf > target/$(DIAGRAM_TARGET).pdf

$(RM):
	cd build; pdflatex ../src/$(RM).tex
	mv build/$(RM).pdf target

$(DESC_TARGET):
	cd build; pdflatex ../src/$(DESC_TARGET).tex
	mv build/$(DESC_TARGET).pdf target

brute: all
	cd target; tar czf brute.tar.gz $(DIAGRAM_TARGET).pdf $(DESC_TARGET).pdf